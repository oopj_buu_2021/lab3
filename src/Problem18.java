import java.util.Scanner;

public class Problem18 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please select star type [1-4,5 is Exit]: ");
        int o = sc.nextInt();
        switch (o) {
            case 1:
                System.out.print("Please input number: ");
                int num1 = sc.nextInt();
                for (int i = 1; i <= num1; i++) {
                    for (int j = 0; j < i; j++) {
                        System.out.print("*");

                    }
                    System.out.println("");

                }
                break;
            case 2:
                System.out.print("Please input number: ");
                int num2 = sc.nextInt();
                for (int i = num2; i >= 0; i--) {
                    for (int j = 0; j < i; j++) {
                        System.out.print("*");

                    }
                    System.out.println("");
                }
                break;
            case 3:
                System.out.print("Please input number: ");
                int num3 = sc.nextInt();
                for (int i = 0; i < num3; i++) {
                    for (int j = 0; j < num3; j++) {
                        if (j >= i) {
                            System.out.print("*");

                        } else {
                            System.out.print(" ");
                        }

                    }
                    System.out.println();
                }
                break;
            case 4:
                System.out.print("Please input number: ");
                int num4 = sc.nextInt();
                for (int i = num4; i >= 0; i--) {
                    for (int j = 0; j < num4; j++) {
                        if (j >= i) {
                            System.out.print("*");

                        } else {
                            System.out.print(" ");
                        }

                    }
                    System.out.println();
                }
                break;
            case 5:
                System.out.println("Bye bye!!!");
                break;

            default:
                System.out.println("Error: Please input number between 1-5");
                break;
        }
        sc.close();
    }

}
