import java.util.Scanner;

public class Problem4 {
    public static void main(String[] args) {
        int sum = 0,P = 1,n = 0;
        float avg = 0f;
        Scanner sc = new Scanner(System.in);
        while (P != 0) {
            System.out.print("Please input number: ");
            int num = sc.nextInt();
            P = num;
            if (P == 0) {
                System.out.print("Bye");
                break;
            }
            sum = sum + num;
            n++;
            avg = sum/n;
            System.out.println("Sum: " + (sum) + " Avg: " + avg);
        }
        sc.close();
    }
}
