import java.util.Scanner;

public class Problem9While {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input n: ");
        int num = sc.nextInt();
        int i = 1;
        while (i < (num+1)) {
            System.out.println("");
            int j = 1;
            while (j < (num+1)) {
                System.out.print(j);
                j++;

            }
            i++;
        }
        sc.close();
    }

}
